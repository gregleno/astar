package astar

import (
	"gitlab.com/gleno/astar/drivers"
)

// Encoders bla
type Encoders struct {
	astar     drivers.AStar
	left      int
	right     int
	lastLeft  int
	lastRight int
}

// NewEncoders creates a new Encoders
func NewEncoders(a drivers.AStar) *Encoders {
	e := new(Encoders)
	e.astar = a
	e.lastLeft, e.lastRight = a.ReadEncoders()
	return e
}

// ReadEncoders reads and returns the encoder values
func (e *Encoders) ReadEncoders() (int, int) {
	left, right := e.astar.ReadEncoders()

	diffLeft := (left - e.lastLeft + 0x10000) % 0x10000
	if diffLeft >= 0x8000 {
		diffLeft -= 0x10000
	}

	diffRight := (right - e.lastRight + 0x10000) % 0x10000
	if diffRight >= 0x8000 {
		diffRight -= 0x10000
	}

	e.left += diffLeft
	e.right += diffRight

	e.lastLeft = left
	e.lastRight = right
	return e.left, e.right
}
