package astar

import (
	"math"
	"sync"
	"time"
)

// Odometer is a blabla
type Odometer struct {
	encoders      *Encoders
	lastSituation Situation
	mutex         *sync.Mutex
}

// Situation represents the current location, orientation and speed of the robot
type Situation struct {
	Time         time.Time
	X            float64
	Y            float64
	Yaw          float64
	UnboundedYaw float64
	Omega        float64
	Distance     float64
	SpeedLeft    float64
	SpeedRight   float64
	Velocity     float64
	LeftEncoder  int
	RightEncoder int
}

// NewOdometer creates a new Odometer
func NewOdometer(e *Encoders) *Odometer {
	o := &Odometer{encoders: e, mutex: &sync.Mutex{}}
	o.ResetOdometry()
	return o
}

func boundAngle(angle float64) float64 {
	return math.Mod(angle, 2*math.Pi)
}

// WheelDistance represents the width between wheels
const WheelDistance float64 = 0.1425

// DistancePerTick represents the distance travelled for per encoder click
const DistancePerTick float64 = .000152505

// ReadSituation reads and returns the current Situation
func (o *Odometer) ReadSituation() Situation {
	o.mutex.Lock()
	defer o.mutex.Unlock()
	left, right := o.encoders.ReadEncoders()

	lastSituation := o.lastSituation
	now := time.Now()
	deltaTime := now.Sub(lastSituation.Time)
	deltaLeft := left - lastSituation.LeftEncoder
	deltaRight := right - lastSituation.RightEncoder

	distLeft := float64(deltaLeft) * DistancePerTick
	distRight := float64(deltaRight) * DistancePerTick
	distanceCenter := (distLeft + distRight) / 2.
	o.lastSituation.Distance += distanceCenter

	o.lastSituation.X += float64(distanceCenter) * math.Cos(lastSituation.Yaw)
	o.lastSituation.Y += float64(distanceCenter) * math.Sin(lastSituation.Yaw)

	deltaYaw := (distLeft - distRight) / WheelDistance
	o.lastSituation.Yaw = boundAngle(lastSituation.Yaw + deltaYaw)
	o.lastSituation.UnboundedYaw = lastSituation.UnboundedYaw + deltaYaw
	if deltaTime != 0 {
		o.lastSituation.Omega = float64(deltaYaw) / deltaTime.Seconds()
		o.lastSituation.SpeedLeft = float64(deltaLeft) / deltaTime.Seconds()
		o.lastSituation.SpeedRight = float64(deltaRight) / deltaTime.Seconds()
		o.lastSituation.Velocity = float64(distLeft+distRight) / deltaTime.Seconds()
	}
	o.lastSituation.LeftEncoder, o.lastSituation.RightEncoder = left, right
	o.lastSituation.Time = now
	return o.lastSituation
}

// ResetOdometry zeroes the acutal Situation
func (o *Odometer) ResetOdometry() {
	o.mutex.Lock()
	defer o.mutex.Unlock()
	o.lastSituation = Situation{}
	o.lastSituation.Time = time.Now()
}
