package astar_test

import (
	"math"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/mock_drivers"
)

func TestDistance(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockAstar := mock_drivers.NewMockAStar(mockCtrl)
	mockAstar.EXPECT().ReadEncoders().Return(0, 0)

	e := astar.NewEncoders(mockAstar)
	o := astar.NewOdometer(e)
	mockAstar.EXPECT().ReadEncoders().Return(1000, 1000)
	situation := o.ReadSituation()
	assert.Equal(t, 1000*astar.DistancePerTick, situation.Distance, "Wrong distance values")
}

func TestYaw(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockAstar := mock_drivers.NewMockAStar(mockCtrl)
	mockAstar.EXPECT().ReadEncoders().Return(0, 0)

	e := astar.NewEncoders(mockAstar)
	o := astar.NewOdometer(e)

	halfCircle := astar.WheelDistance * math.Pi / 2
	mockAstar.EXPECT().ReadEncoders().Return(int(halfCircle/astar.DistancePerTick), -int(halfCircle/astar.DistancePerTick))
	situation := o.ReadSituation()
	assert.InDelta(t, math.Pi, situation.Yaw, 0.002, "Wrong yaw")

	quarterCircle := halfCircle / 2
	mockAstar.EXPECT().ReadEncoders().Return(int(quarterCircle/astar.DistancePerTick), -int(quarterCircle/astar.DistancePerTick))
	situation = o.ReadSituation()
	assert.InDelta(t, math.Pi/2, situation.Yaw, 0.002, "Wrong yaw")
}
