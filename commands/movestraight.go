package commands

import (
	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/drivers"
	"math"
	"time"
)

type moveStraight struct {
	*MotionCommand
	distance         float64
	maxSpeed         float64
	pidYawCorrection PID
}

// NewMoveStraight creates a new MotionCommand which move the robot in a straight line
func NewMoveStraight(odometer *astar.Odometer, astar drivers.AStar, distance float64, maxSpeed float64) *MotionCommand {
	motionCommand := NewMotionCommand(odometer, astar)
	moveStraight := &moveStraight{motionCommand, distance, maxSpeed, *NewPID(2., 2., 0.0)}
	motionCommand.mover = moveStraight
	return moveStraight.MotionCommand
}

func (m *moveStraight) move() bool {
	situation := m.odometer.ReadSituation()
	doneDistance := situation.Distance - m.initialSituation.Distance
	remainingDistance := m.distance - doneDistance
	if m.waitingForMotorsToStop {
		return situation.SpeedLeft == 0 && situation.SpeedRight == 0
	} else if math.Abs(remainingDistance) < 0.001 ||
		math.Abs(remainingDistance)-math.Abs(m.distance) > 0.01 {
		m.astar.Motors(0, 0)
		m.lastLeftCommand, m.lastRightCommand = 0, 0
		m.waitingForMotorsToStop = true
		// Wait for the robot to stop to tell that we are done
		return situation.SpeedLeft == 0 && situation.SpeedRight == 0
	}
	cmd := m.getMotorCommand(remainingDistance, (m.lastLeftCommand+m.lastRightCommand)/2, m.maxSpeed)
	left, right := m.adjustForYaw(situation.Yaw, m.initialSituation.Yaw, cmd, time.Now())
	left = capAcceleration(m.lastLeftCommand, left)
	right = capAcceleration(m.lastRightCommand, right)
	m.astar.Motors(left, right)
	m.lastLeftCommand, m.lastRightCommand = left, right
	return false
}

func (m *moveStraight) getMotorCommand(remainingDistance, lastCommand, maxSpeed float64) float64 {
	var cmd float64
	maxSpeed = math.Abs(maxSpeed)
	lastCommand = math.Abs(lastCommand)

	const speedReductionDistance = 0.2
	if math.Abs(remainingDistance) > speedReductionDistance {
		cmd = maxSpeed
	} else {
		cmd = maxSpeed * math.Abs(remainingDistance) / speedReductionDistance
	}
	cmd = math.Copysign(cmd, remainingDistance)

	return cmd
}

func (m *moveStraight) adjustForYaw(currentYaw, expectedYaw, cmd float64, currentTime time.Time) (left, right float64) {
	left, right = cmd, cmd
	if expectedYaw-currentYaw > math.Pi {
		expectedYaw = currentYaw + 2.*math.Pi
	} else if expectedYaw-currentYaw < -math.Pi {
		expectedYaw = currentYaw - 2.*math.Pi
	}

	delta := m.pidYawCorrection.GetOutput(expectedYaw, currentYaw, currentTime)
	// When going backward, yaw correction must be inverted
	if cmd < 0 {
		delta = -delta
	}

	left, right = left*(1+delta), right*(1-delta)
	// we do not allow left and right to go in opposite directions
	offset := 0.
	if left*cmd < 0 {
		offset = -left
	} else if right*cmd < 0 {
		offset = -right
	}
	return left + offset, right + offset
}
