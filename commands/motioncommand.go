package commands

import (
	"fmt"
	"math"
	"time"

	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/drivers"
)

// MotionCommand bla
type MotionCommand struct {
	odometer                          *astar.Odometer
	astar                             drivers.AStar
	Done                              chan bool
	initialSituation                  astar.Situation
	lastLeftCommand, lastRightCommand float64
	mover                             mover
	waitingForMotorsToStop            bool
}

type mover interface {
	move() bool
}

// NewMotionCommand creates a new MotionCommand
func NewMotionCommand(odometer *astar.Odometer, astar drivers.AStar) *MotionCommand {
	return &MotionCommand{
		odometer:               odometer,
		astar:                  astar,
		Done:                   make(chan bool, 1),
		initialSituation:       odometer.ReadSituation(),
		waitingForMotorsToStop: false,
	}
}

// Start begins the execution of the MotionCommand asynchronously
func (m *MotionCommand) Start() {
	ticker := time.NewTicker(10 * time.Millisecond)
	inactivityTimeout := time.NewTicker(1 * time.Second)
	lastSituation := m.odometer.ReadSituation()
	go func() {
		for {
			select {
			case <-ticker.C:
				if m.mover == nil || m.mover.move() {
					ticker.Stop()
					inactivityTimeout.Stop()
					m.Done <- true
				}
			case <-inactivityTimeout.C:
				currentSituation := m.odometer.ReadSituation()
				if currentSituation.X == lastSituation.X && currentSituation.Y == lastSituation.Y {
					fmt.Println("Nothing moved since one second, stopping")
					m.Done <- false
					break
				}
				lastSituation = currentSituation
			}
		}
	}()
}

const maxAccelerationCommand = 0.05

func capAcceleration(lastCommand, cmd float64) float64 {
	if math.Abs(lastCommand-cmd) > maxAccelerationCommand {
		return lastCommand + math.Copysign(maxAccelerationCommand, cmd-lastCommand)
	}
	return cmd
}
