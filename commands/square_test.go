// +build !race,!msan
// The tests take too long under the race detector.

package commands

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/drivers"
	"testing"
	"time"
)

func TestSquare(t *testing.T) {
	simu := drivers.NewFakeAStar()
	encoders := astar.NewEncoders(simu)
	odom := astar.NewOdometer(encoders)
	motionCmd := NewSquare(odom, simu, 0.2, 1)
	motionCmd.Start()
	testTimeout := time.AfterFunc(10*time.Second, func() {
		t.Fatal("Command was not finished in time")
	})
	result := <-motionCmd.Done
	testTimeout.Stop()
	assert.True(t, result, "Command was not successful")
	assert.InDelta(t, odom.ReadSituation().Distance, 0.8, 0.01)
}
