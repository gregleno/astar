package commands

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/drivers"
	"testing"
	"time"
)

func TestMoveStraight(t *testing.T) {
	simu := drivers.NewFakeAStar()
	encoders := astar.NewEncoders(simu)
	odom := astar.NewOdometer(encoders)
	motionCmd := NewMoveStraight(odom, simu, 1, 1)
	motionCmd.Start()
	testTimeout := time.AfterFunc(3000*time.Millisecond, func() {
		t.Fatal("Command was not finished in time")
	})
	result := <-motionCmd.Done
	testTimeout.Stop()
	assert.True(t, result, "Command was not successful")
	assert.InDelta(t, odom.ReadSituation().Distance, 1, 0.001)
}
