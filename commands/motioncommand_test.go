package commands

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/drivers"
	"testing"
	"time"
)

func TestStart(t *testing.T) {
	simu := drivers.NewFakeAStar()
	encoders := astar.NewEncoders(simu)
	odom := astar.NewOdometer(encoders)
	motionCmd := NewMotionCommand(odom, simu)
	motionCmd.Start()
	testTimeout := time.AfterFunc(100*time.Millisecond, func() {
		t.Fatal("Command was not finished in time")
	})
	<-motionCmd.Done
	testTimeout.Stop()
}

type ProtoMover struct{ moveMethod func() bool }

func (t ProtoMover) move() bool { return t.moveMethod() }

func TestInactivity(t *testing.T) {
	simu := drivers.NewFakeAStar()
	encoders := astar.NewEncoders(simu)
	odom := astar.NewOdometer(encoders)
	motionCmd := NewMotionCommand(odom, simu)
	mover := struct{ ProtoMover }{}
	mover.moveMethod = func() bool { return false }
	motionCmd.mover = mover
	motionCmd.Start()

	testTimeout := time.AfterFunc(2500*time.Millisecond, func() {
		t.Fatal("Inactivity timeout did not kick in time")
	})
	assert.False(t, <-motionCmd.Done, "command should have returned false")
	testTimeout.Stop()
}
