package commands

import (
	"fmt"
	"math"

	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/drivers"
)

// Encoders bla
type turn struct {
	*MotionCommand
	yaw      float64
	maxSpeed float64
}

// NewTurn crestes a new MotionCommand which turns the robot in place
func NewTurn(odometer *astar.Odometer, astar drivers.AStar, yaw float64, maxSpeed float64) *MotionCommand {
	motionCommand := NewMotionCommand(odometer, astar)
	turn := &turn{motionCommand, yaw, maxSpeed}
	motionCommand.mover = turn
	return turn.MotionCommand
}

func (m *turn) move() bool {
	situation := m.odometer.ReadSituation()
	doneYaw := situation.UnboundedYaw - m.initialSituation.UnboundedYaw
	remainingYaw := m.yaw - doneYaw

	if m.waitingForMotorsToStop {
		return situation.SpeedLeft == 0 && situation.SpeedRight == 0
	} else if math.Abs(remainingYaw) < 0.001 {
		m.astar.Motors(0, 0)
		m.lastLeftCommand, m.lastRightCommand = 0, 0
		// Wait for the robot to stop to tell that we are done
		fmt.Printf("%f %f\n", situation.SpeedLeft, situation.SpeedRight)

		m.waitingForMotorsToStop = true
		// todo this should be done if we have reached this if once
		return situation.SpeedLeft == 0 && situation.SpeedRight == 0
	}
	cmd := m.getMotorCommand(remainingYaw, m.lastLeftCommand, m.maxSpeed)
	left := capAcceleration(m.lastLeftCommand, cmd)
	right := capAcceleration(m.lastRightCommand, -cmd)
	m.astar.Motors(left, right)
	m.lastLeftCommand, m.lastRightCommand = left, right
	return false
}

func (m *turn) getMotorCommand(remainingYaw, lastCommand, maxSpeed float64) float64 {
	var cmd float64
	maxSpeed = math.Abs(maxSpeed)
	lastCommand = math.Abs(lastCommand)

	var speedReductionYaw = 1 * (1 + maxSpeed - 0.5)
	if math.Abs(remainingYaw) > speedReductionYaw || lastCommand == 0 {
		cmd = maxSpeed
	} else {
		cmd = maxSpeed * math.Abs(remainingYaw) / speedReductionYaw
	}
	cmd = math.Min(cmd, 1)
	cmd = math.Copysign(cmd, remainingYaw)

	return cmd
}
