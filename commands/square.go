package commands

import (
	"math"

	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/drivers"
)

type square struct {
	*MotionCommand
	length       float64
	maxSpeed     float64
	commands     []int
	currentMover *MotionCommand
}

const (
	move = iota
	rotate
)

// NewSquare creates a new MotionCommand which makes the robot follow a square
func NewSquare(odometer *astar.Odometer, astar drivers.AStar, length float64, maxSpeed float64) *MotionCommand {
	motionCommand := NewMotionCommand(odometer, astar)
	square := &square{motionCommand, length, maxSpeed, []int{move, rotate, move, rotate, move, rotate, move, rotate}, nil}
	motionCommand.mover = square
	square.currentMover = square.getNextCommand()
	return square.MotionCommand
}

func (m *square) move() bool {
	if m.currentMover != nil {
		if m.currentMover.mover.move() {
			m.currentMover = m.getNextCommand()
			// s := m.odometer.ReadSituation()
		}
		return false
	}
	return true
}

func (m *square) getNextCommand() *MotionCommand {
	var cmd *MotionCommand
	if len(m.commands) > 0 {
		switch m.commands[0] {
		case move:
			cmd = NewMoveStraight(m.odometer, m.astar, m.length, m.maxSpeed)
		case rotate:
			cmd = NewTurn(m.odometer, m.astar, math.Pi/2, m.maxSpeed/2)
		default:
			cmd = nil
		}
		m.commands = m.commands[1:]
	}
	return cmd
}
