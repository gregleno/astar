package drivers

import (
	"encoding/binary"
	"math"
	"sync"
	"time"

	"gobot.io/x/gobot"
	"gobot.io/x/gobot/drivers/i2c"
)

const astarRegisterLeds = 0x0
const astarAddress = 0x14
const astarRegisterButtons = 0x3
const astarRegisterMotors = 0x6
const astarRegisterBattery = 0xa
const astarRegisterAnalog = 0xc
const astarRegisterPlayNotes = 0x18
const astarRegisterEncoders = 0x27

const minimumMotorValueForMovement float64 = 25.
const maximumMotorValue float64 = 400.
const minimumCommandForMovement = minimumMotorValueForMovement / maximumMotorValue

// AStarDriver is a Gobot Driver for an A-Star Pololu Romi 32U4 board
type AStarDriver struct {
	name       string
	connector  i2c.Connector
	connection i2c.Connection
	i2c.Config
	gobot.Commander
	mutex *sync.Mutex
}

// AStar is an interface to AStar features
type AStar interface {
	Leds(red, yellow, green bool)
	Motors(left, right float64)
	PlayNotes(notes string)
	ReadBattery() int
	ReadButtons() (bool, bool, bool)
	ReadAnalog() []int
	ReadEncoders() (int, int)
}

// NewAStarDriver creates a new NewAStarDriver.
//
// Params:
//		conn i2c.Connector - the Adaptor to use with this Driver
//
func NewAStarDriver(a i2c.Connector, options ...func(i2c.Config)) *AStarDriver {
	b := &AStarDriver{
		name:      gobot.DefaultName("ASTAR"),
		Commander: gobot.NewCommander(),
		connector: a,
		Config:    i2c.NewConfig(),
		mutex:     &sync.Mutex{},
	}

	for _, option := range options {
		option(b)
	}

	return b
}

// Name returns the Name for the Driver
func (b *AStarDriver) Name() string { return b.name }

// SetName sets the Name for the Driver
func (b *AStarDriver) SetName(n string) { b.name = n }

// Connection returns the connection for the Driver
func (b *AStarDriver) Connection() gobot.Connection { return b.connector.(gobot.Connection) }

// Start starts the Driver up, and writes start command
func (b *AStarDriver) Start() (err error) {
	bus := b.GetBusOrDefault(b.connector.GetDefaultBus())
	address := b.GetAddressOrDefault(astarAddress)

	b.connection, err = b.connector.GetConnection(address, bus)
	return err
}

// Halt returns true if device is halted successfully
func (b *AStarDriver) Halt() (err error) { return }

// Leds sets led status using r,g,b params
func (b *AStarDriver) Leds(red, yellow, green bool) {
	data := make([]byte, 3)
	if yellow {
		data[0] = 1
	}
	if green {
		data[1] = 1
	}
	if red {
		data[2] = 1
	}
	b.write(astarRegisterLeds, data)
}

// Motors sets the motors values
func (b *AStarDriver) Motors(left, right float64) {
	leftCmd := convertMotorCommandToValue(left)
	rightCmd := convertMotorCommandToValue(right)
	data := make([]byte, 4)
	binary.LittleEndian.PutUint16(data[0:], uint16(leftCmd))
	binary.LittleEndian.PutUint16(data[2:], uint16(rightCmd))
	b.write(astarRegisterMotors, data)
}

func adjustMotorCommand(cmd float64) float64 {
	if cmd == 0 {
		return 0
	}
	cmd = math.Max(-1, cmd)
	cmd = math.Min(1, cmd)
	absoluteValue := minimumMotorValueForMovement/maximumMotorValue + (maximumMotorValue-minimumMotorValueForMovement)/maximumMotorValue*math.Abs(cmd)
	return math.Copysign(absoluteValue, cmd)
}

func convertMotorCommandToValue(cmd float64) uint16 {
	return uint16(adjustMotorCommand(cmd) * maximumMotorValue)
}

// PlayNotes plays a set of notess
func (b *AStarDriver) PlayNotes(notes string) {
	data := make([]byte, 16)
	data[0] = 1
	copy(data[1:], notes)
	b.write(astarRegisterPlayNotes, data)
}

// ReadBattery returns the battery value
func (b *AStarDriver) ReadBattery() int {
	return int(binary.LittleEndian.Uint16(b.read(astarRegisterBattery, 2)))
}

// ReadButtons returns the current buttons states
func (b *AStarDriver) ReadButtons() (bool, bool, bool) {
	data := b.read(astarRegisterButtons, 3)
	one := data[0] == 1
	two := data[1] == 1
	three := data[2] == 1
	return one, two, three
}

// ReadAnalog returns the measured analog input from the 32u4
func (b *AStarDriver) ReadAnalog() []int {
	analog := b.read(astarRegisterAnalog, 12)
	data := make([]int, 6)
	for index := range data {
		data[index] = int(binary.LittleEndian.Uint16(analog[index*2:]))
	}
	return data
}

// ReadEncoders reads the encoders values
func (b *AStarDriver) ReadEncoders() (int, int) {
	data := b.read(astarRegisterEncoders, 4)
	left := binary.LittleEndian.Uint16(data[0:])
	right := binary.LittleEndian.Uint16(data[2:])
	return int(left), int(right)
}

func (b *AStarDriver) write(reg uint8, data []byte) (err error) {
	b.mutex.Lock()
	defer b.mutex.Unlock()
	b.connection.WriteBlockData(reg, data)
	time.Sleep(100 * time.Microsecond)
	return nil
}

func (b *AStarDriver) read(reg uint8, n int) []byte {
	// The AVR's TWI module can't handle a quick write->read transition,
	// since the STOP interrupt will occasionally happen after the START
	// condition, and the TWI module is disabled until the interrupt can
	// be processed.
	//
	// A delay of 0.0001 (100 us) after each write is enough to account
	// for the worst-case situation in our example code.
	b.mutex.Lock()
	defer b.mutex.Unlock()
	b.connection.WriteByte(reg)
	time.Sleep(100 * time.Microsecond)
	data := make([]byte, n)

	b.connection.Read(data)

	return data
}
