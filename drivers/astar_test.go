package drivers

import (
	"bytes"
	"testing"

	"gobot.io/x/gobot"
	"gobot.io/x/gobot/drivers/i2c"
	"gobot.io/x/gobot/gobottest"
)

var _ gobot.Driver = (*AStarDriver)(nil)

// --------- HELPERS
func initTestAStarDriver() (driver *AStarDriver) {
	driver, _ = initTestAStarDriverWithStubbedAdaptor()
	return
}

func initTestAStarDriverWithStubbedAdaptor() (*AStarDriver, *i2cTestAdaptor) {
	adaptor := newI2cTestAdaptor()
	return NewAStarDriver(adaptor), adaptor
}

// --------- TESTS

func TestNewAStarDriver(t *testing.T) {
	// Does it return a pointer to an instance of AStarDriver?
	var astar interface{} = NewAStarDriver(newI2cTestAdaptor())
	_, ok := astar.(*AStarDriver)
	if !ok {
		t.Errorf("NewAStarDriver() should have returned a *AStarDriver")
	}
}

func TestAStarDriver(t *testing.T) {
	astar := initTestAStarDriver()
	gobottest.Refute(t, astar.Connection(), nil)
}

func TestAStarDriverStart(t *testing.T) {
	astar, _ := initTestAStarDriverWithStubbedAdaptor()
	gobottest.Assert(t, astar.Start(), nil)
}

func TestAStarDriverHalt(t *testing.T) {
	bmp280 := initTestAStarDriver()

	gobottest.Assert(t, bmp280.Halt(), nil)
}

func TestAStarDriverReads(t *testing.T) {
	astar, adaptor := initTestAStarDriverWithStubbedAdaptor()
	adaptor.i2cReadImpl = func(b []byte) (int, error) {
		buf := new(bytes.Buffer)

		// Values produced by dumping data from actual device
		if adaptor.written[len(adaptor.written)-1] == astarRegisterAnalog {
			buf.Write([]byte{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 3})
		} else if adaptor.written[len(adaptor.written)-1] == astarRegisterButtons {
			buf.Write([]byte{1, 0, 1})
		} else if adaptor.written[len(adaptor.written)-1] == astarRegisterBattery {
			buf.Write([]byte{181, 21})
		} else if adaptor.written[len(adaptor.written)-1] == astarRegisterEncoders {
			buf.Write([]byte{75, 41, 90, 47})
		}
		copy(b, buf.Bytes())
		return buf.Len(), nil
	}
	astar.Start()
	analog := astar.ReadAnalog()
	gobottest.Assert(t, analog, []int{0, 0, 0, 0, 0, 1023})
	battery := astar.ReadBattery()
	gobottest.Assert(t, battery, 5557)
	one, two, three := astar.ReadButtons()
	gobottest.Assert(t, one, true)
	gobottest.Assert(t, two, false)
	gobottest.Assert(t, three, true)
	left, right := astar.ReadEncoders()
	gobottest.Assert(t, left, 10571)
	gobottest.Assert(t, right, 12122)
}

func TestAStarDriverLeds(t *testing.T) {
	astar, adaptor := initTestAStarDriverWithStubbedAdaptor()
	astar.Start()
	astar.Leds(true, false, true)
	gobottest.Assert(t, adaptor.written, []byte{0, 0, 1, 1})

	astar, adaptor = initTestAStarDriverWithStubbedAdaptor()
	astar.Start()
	astar.Leds(false, true, false)
	gobottest.Assert(t, adaptor.written, []byte{0, 1, 0, 0})
}

func TestAStarDriverMotors(t *testing.T) {
	astar, adaptor := initTestAStarDriverWithStubbedAdaptor()
	astar.Start()
	astar.Motors(-0.5, 0.5)
	gobottest.Assert(t, adaptor.written, []byte{6, 44, 255, 212, 0})
}

func TestAStarDriverPlayNotes(t *testing.T) {
	astar, adaptor := initTestAStarDriverWithStubbedAdaptor()
	astar.Start()
	astar.PlayNotes("abc")
	gobottest.Assert(t, adaptor.written, []byte{24, 1, 97, 98, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0})
}

func TestAStarDriverSetName(t *testing.T) {
	b := initTestAStarDriver()
	b.SetName("TESTME")
	gobottest.Assert(t, b.Name(), "TESTME")
}

func TestAStarDriverOptions(t *testing.T) {
	b := NewAStarDriver(newI2cTestAdaptor(), i2c.WithBus(2))
	gobottest.Assert(t, b.GetBusOrDefault(1), 2)
}

func TestConvertMotorCommandToValue(t *testing.T) {
	gobottest.Assert(t, convertMotorCommandToValue(0), uint16(0))
	gobottest.Assert(t, convertMotorCommandToValue(0.000001), uint16(minimumMotorValueForMovement))
	gobottest.Assert(t, convertMotorCommandToValue(1), uint16(400))
	gobottest.Assert(t, convertMotorCommandToValue(0.5), uint16(212))
	gobottest.Assert(t, convertMotorCommandToValue(-0.5), uint16(65324))
	gobottest.Assert(t, convertMotorCommandToValue(-1), uint16(65136))
	gobottest.Assert(t, convertMotorCommandToValue(2), uint16(400))
	gobottest.Assert(t, convertMotorCommandToValue(-2), uint16(65136))
}
