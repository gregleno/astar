package drivers

import (
	"math"
	"time"
)

type fakeAStar struct {
	encoderLeft, encoderRight int
	cmdLeft, cmdRight         float64
	lastEncoderUpdateTime     time.Time
}

// NewFakeAStar creates a new FakeAStar for testing
func NewFakeAStar() AStar {
	f := fakeAStar{}
	f.lastEncoderUpdateTime = time.Now()
	return &f
}

// Leds faked
func (f *fakeAStar) Leds(red, yellow, green bool) {
}

// PlayNotes faked
func (f *fakeAStar) PlayNotes(notes string) {
}

// Motors faked
func (f *fakeAStar) Motors(left, right float64) {
	f.updateEncoders()
	f.cmdLeft = adjustMotorCommand(left)
	f.cmdRight = adjustMotorCommand(right)
}

// ReadBattery faked
func (f *fakeAStar) ReadBattery() int {
	return 7000
}

// ReadButtons faked
func (f *fakeAStar) ReadButtons() (bool, bool, bool) {
	return true, true, true
}

// ReadAnalog faked
func (f *fakeAStar) ReadAnalog() []int {
	return []int{0}
}

// ReadEncoders faked
func (f *fakeAStar) ReadEncoders() (int, int) {
	f.updateEncoders()
	return f.encoderLeft, f.encoderRight
}

func getRealCmd(cmd float64) float64 {
	if math.Abs(cmd) < minimumCommandForMovement {
		return 0
	}
	return cmd
}

func (f *fakeAStar) updateEncoders() {
	// The maximum speed in ticks per second
	maxSpeed := 1440 * 150 / 60.

	currentTime := time.Now()
	deltaTime := currentTime.Sub(f.lastEncoderUpdateTime).Seconds()
	left := f.encoderLeft + int(getRealCmd(f.cmdLeft)*maxSpeed*deltaTime)
	right := f.encoderRight + int(getRealCmd(f.cmdRight)*maxSpeed*deltaTime)

	f.lastEncoderUpdateTime = currentTime
	f.encoderLeft, f.encoderRight = left, right
}
