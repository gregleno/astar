package main

import (
	"flag"
	"gitlab.com/gleno/astar/drivers"
	"gobot.io/x/gobot/platforms/raspi"
)

func main() {
	notes := flag.String("notes", "", "The notes to play")
	flag.Parse()
	r := raspi.NewAdaptor()
	r.Connect()
	astar := drivers.NewAStarDriver(r)
	astar.Start()
	astar.PlayNotes(*notes)
}
