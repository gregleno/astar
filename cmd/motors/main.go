package main

import (
	"flag"
	"gitlab.com/gleno/astar/drivers"
	"gobot.io/x/gobot/platforms/raspi"
)

func main() {
	left := flag.Float64("left", 0, "The left motor value between -1 and 1")
	right := flag.Float64("right", 0, "The right motor value between -1 and 1")
	flag.Parse()
	r := raspi.NewAdaptor()
	r.Connect()
	astar := drivers.NewAStarDriver(r)
	astar.Start()
	astar.Motors(*left, *right)
}
