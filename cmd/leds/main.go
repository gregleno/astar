package main

import (
	"flag"
	"gitlab.com/gleno/astar/drivers"
	"gobot.io/x/gobot/platforms/raspi"
)

func main() {
	yellow := flag.Bool("yellow", false, "The Yellow led")
	red := flag.Bool("red", false, "The Red led")
	green := flag.Bool("green", false, "The Green led")
	flag.Parse()
	r := raspi.NewAdaptor()
	r.Connect()
	astar := drivers.NewAStarDriver(r)
	astar.Start()
	astar.Leds(*red, *yellow, *green)
}
