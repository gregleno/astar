package main

import (
	"flag"
	"fmt"
	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/commands"
	"gitlab.com/gleno/astar/drivers"
	"gobot.io/x/gobot/platforms/raspi"
)

func main() {
	distance := flag.Float64("distance", 0, "The left motor value")
	yaw := flag.Float64("yaw", 0, "The Yaw value")
	speed := flag.Float64("speed", 0, "The right motor value")
	square := flag.Bool("square", false, "The right motor value")
	flag.Parse()

	r := raspi.NewAdaptor()
	r.Connect()
	astarDriver := drivers.NewAStarDriver(r)
	astarDriver.Start()
	encoders := astar.NewEncoders(astarDriver)
	odom := astar.NewOdometer(encoders)
	var command *commands.MotionCommand
	if *yaw != 0 {
		command = commands.NewTurn(odom, astarDriver, *yaw, *speed)
	} else if *square {
		command = commands.NewSquare(odom, astarDriver, *distance, *speed)
	} else if *distance != 0 {
		command = commands.NewMoveStraight(odom, astarDriver, *distance, *speed)
	}
	command.Start()
	success := <-command.Done
	if !success {
		fmt.Println("Could not complete move, aborting")
	}

	fmt.Println("Finished")
	fmt.Println(odom.ReadSituation().Distance)
	fmt.Println(odom.ReadSituation().Yaw)
	fmt.Println(odom.ReadSituation().Yaw * 180 / 3.14159)
}
