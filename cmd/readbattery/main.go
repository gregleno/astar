package main

import (
	"fmt"
	"gitlab.com/gleno/astar/drivers"
	"gobot.io/x/gobot/platforms/raspi"
)

func main() {
	r := raspi.NewAdaptor()
	r.Connect()
	astar := drivers.NewAStarDriver(r)
	astar.Start()
	fmt.Println(astar.ReadBattery())
}
