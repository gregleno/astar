package astar_test

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gleno/astar"
	"gitlab.com/gleno/astar/mock_drivers"
)

func TestReadEncoders(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockAstar := mock_drivers.NewMockAStar(mockCtrl)
	mockAstar.EXPECT().ReadEncoders().Return(0, 0)
	mockAstar.EXPECT().ReadEncoders().Return(1, 1)

	e := astar.NewEncoders(mockAstar)
	left, right := e.ReadEncoders()
	assert.EqualValues(t, []int{1, 1}, []int{left, right}, "Wrong encoder values")
}
func TestOverrun(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockAstar := mock_drivers.NewMockAStar(mockCtrl)
	before := 0x10000 - 1
	mockAstar.EXPECT().ReadEncoders().Return(before, before)
	mockAstar.EXPECT().ReadEncoders().Return(1, 1)

	e := astar.NewEncoders(mockAstar)
	left, right := e.ReadEncoders()
	assert.EqualValues(t, []int{2, 2}, []int{left, right}, "Overrun not managed")
}
func TestUnderrun(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockAstar := mock_drivers.NewMockAStar(mockCtrl)
	after := 0x10000 - 1
	mockAstar.EXPECT().ReadEncoders().Return(1, 1)
	mockAstar.EXPECT().ReadEncoders().Return(after, after)

	e := astar.NewEncoders(mockAstar)
	left, right := e.ReadEncoders()
	assert.EqualValues(t, []int{-2, -2}, []int{left, right}, "Overrun not managed")
}
