module gitlab.com/gleno/astar

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/eclipse/paho.mqtt.golang v1.1.1 // indirect
	github.com/golang/mock v1.1.1
	github.com/hashicorp/go-multierror v1.0.0 // indirect
	github.com/pkg/errors v0.8.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sigurn/crc8 v0.0.0-20160107002456-e55481d6f45c // indirect
	github.com/sigurn/utils v0.0.0-20151230205143-f19e41f79f8f // indirect
	github.com/stretchr/testify v1.2.2
	gobot.io/x/gobot v1.12.0
	golang.org/x/net v0.0.0-20170915142106-8351a756f30f // indirect
	periph.io/x/periph v3.3.0+incompatible // indirect
)
