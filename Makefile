PROJECT_NAME := "astar"
PKG := "gitlab.com/gleno/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... )
GO_FILES := $(shell find . -name '*.go' | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint:
	@golint -set_exit_status ${PKG_LIST}

test:
	@go test -v ${PKG_LIST}

test-report:
	@go test ${PKG_LIST} 2>&1 | go-junit-report > report.xml

race: dep
	@go test -race -short ${PKG_LIST}

msan: dep
	@go test -msan -short ${PKG_LIST}

coverage:
	./tools/coverage.sh;

coverhtml:
	./tools/coverage.sh html;

dep:
	@go get -v -d ./...

build: dep
	@go install -v ${PKG_LIST}

clean: ## Remove previous build
	@rm -f ${PROJECT_NAME}

help:
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
